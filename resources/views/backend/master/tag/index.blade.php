@extends('backend.layouts.app')

@section('title', app_name() . ' | Tag Management')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Tag Management
                    </h4>
                </div><!--col-->

                <div class="col-sm-7 pull-right">
                    @include('backend.master.tag.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Tag Name</th>
                                <th>Number of Products</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($tags as $tag)
                                <tr>
                                    <td>{{ $tag->name }}</td>
                                    <td>{{ $tag->products->count() }}</td>
                                    <td>@include('backend.master.tag.includes.actions', ['tag' => $tag])</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {!! $tags->total() !!} products total
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $tags->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->
    </div><!--card-->
@endsection
