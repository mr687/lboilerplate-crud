<?php

namespace App\Events\Backend\Master\Product;

use Illuminate\Queue\SerializesModels;

class ProductUpdated
{
    use SerializesModels;

    public $product;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;
    }
}
