<?php

use App\Models\Master\Vendor;
use Illuminate\Database\Seeder;

class VendorTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Vendor::class, 10)->create();

        $this->enableForeignKeys();
    }
}
