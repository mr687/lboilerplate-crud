<?php


namespace App\Http\Controllers\Api\Master;


use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected function resultJson($data, $code, $msg)
    {
        return response()->json([
            'msg' => $msg,
            'result' => $data
        ], $code);
    }
}
