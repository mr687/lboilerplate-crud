<?php

use App\Http\Controllers\Backend\Master\Product\ProductController;
use App\Http\Controllers\Backend\Master\Tag\TagController;
use App\Http\Controllers\Backend\Master\Vendor\VendorController;

Route::group([
    'prefix' => 'master',
    'as' => 'master.',
    'namespace' => 'Master',
    'middleware' => 'role:'.config('access.users.admin_role')
], function(){
    // Product Management
    Route::group(['namespace' => 'Product'], function() {

        // CRUD
        Route::get('product', [ProductController::class, 'index'])->name('product.index');
        Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
        Route::post('product', [ProductController::class, 'store'])->name('product.store');

        Route::group(['prefix' => 'product/{product}'], function() {
            Route::get('edit', [ProductController::class, 'edit'])->name('product.edit');
            Route::patch('/', [ProductController::class, 'update'])->name('product.update');
            Route::delete('/', [ProductController::class, 'destroy'])->name('product.destroy');
        });

    });

    // Vendor Management
    Route::group(['namespace' => 'Vendor'], function() {

        // CRUD
        Route::get('vendor', [VendorController::class, 'index'])->name('vendor.index');
        Route::get('vendor/create', [VendorController::class, 'create'])->name('vendor.create');
        Route::post('vendor', [VendorController::class, 'store'])->name('vendor.store');

        Route::group(['prefix' => 'vendor/{vendor}'], function() {
            Route::get('', [VendorController::class, 'show'])->name('vendor.show');
            Route::get('edit', [VendorController::class, 'edit'])->name('vendor.edit');
            Route::patch('/', [VendorController::class, 'update'])->name('vendor.update');
            Route::delete('/', [VendorController::class, 'destroy'])->name('vendor.destroy');
        });

    });

    // Tag Management
    Route::group(['namespace' => 'Tag'], function() {

        // CRUD
        Route::get('tag', [TagController::class, 'index'])->name('tag.index');
        Route::get('tag/create', [TagController::class, 'create'])->name('tag.create');
        Route::post('tag', [TagController::class, 'store'])->name('tag.store');

        Route::group(['prefix' => 'tag/{tag}'], function() {
            Route::get('', [TagController::class, 'show'])->name('tag.show');
            Route::get('edit', [TagController::class, 'edit'])->name('tag.edit');
            Route::patch('/', [TagController::class, 'update'])->name('tag.update');
            Route::delete('/', [TagController::class, 'destroy'])->name('tag.destroy');
        });

    });
});
