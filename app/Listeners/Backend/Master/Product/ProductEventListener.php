<?php

namespace App\Listeners\Backend\Master\Product;

use App\Events\Backend\Master\Product\ProductCreated;
use App\Events\Backend\Master\Product\ProductUpdated;
use App\Events\Backend\Master\Product\ProductDeleted;

class ProductEventListener
{
    public function onCreated($event)
    {
        logger('Product Created');
    }

    public function onDeleted($event)
    {
        logger('Product Deleted');
    }

    public function onUpdated($event)
    {
        logger('Product Updated');
    }

    public function subscribe($events)
    {
        $events->listen(
            ProductCreated::class,
            'App\Listeners\Backend\Master\Product\ProductEventListener@onCreated'
        );

        $events->listen(
            ProductUpdated::class,
            'App\Listeners\Backend\Master\Product\ProductEventListener@onUpdated'
        );

        $events->listen(
            ProductDeleted::class,
            'App\Listeners\Backend\Master\Product\ProductEventListener@onDeleted'
        );
    }
}
