<?php


namespace App\Http\Requests\Api\Master\Tag;


use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
