<?php


namespace App\Models\Master\Traits\Relationship;


use App\Models\Master\Product;

trait VendorRelationship
{
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
