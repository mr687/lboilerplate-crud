@extends('backend.layouts.app')

@section('title', app_name() . ' | Product Management')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Product Management
                </h4>
            </div><!--col-->

            <div class="col-sm-7 pull-right">
                @include('backend.master.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Vendor Name</th>
                            <th>Price</th>
                            <th>Tags</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>

                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{ ucwords($product->name) }}</td>
                                    <td>{{ $product->vendor->name }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>
                                        @foreach($product->tags as $tag)
                                            <span class="badge badge-info">{{$tag->name}}</span>
                                        @endforeach
                                    </td>
                                    <td>@include('backend.master.product.includes.actions', ['product'=>$product])</td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $products->total() !!} products total
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $products->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->

    </div><!--card-body-->
</div><!--card-->
@endsection
