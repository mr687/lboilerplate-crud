<?php


namespace App\Http\Requests\Backend\Master\Tag;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreTagRequest extends FormRequest
{

    public function authorize()
    {
        return $this->user()->isAdmin();
    }


    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('tags')]
        ];
    }
}
