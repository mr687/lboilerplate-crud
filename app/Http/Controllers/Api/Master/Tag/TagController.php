<?php

namespace App\Http\Controllers\Api\Master\Tag;

use App\Http\Controllers\Api\Master\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Master\Tag\ManageTagRequest;
use App\Http\Requests\Api\Master\Tag\StoreTagRequest;
use App\Http\Requests\Api\Master\Tag\UpdateTagRequest;
use App\Models\Master\Tag;
use App\Repositories\Backend\Master\TagRepository;
use Illuminate\Http\Request;

class TagController extends BaseController
{
    protected $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function index(ManageTagRequest $request)
    {
        return $this->resultJson(
            $this->tagRepository
            ->orderBy('id')
            ->paginate(25),
            200,
            'OK'
        );
    }

    public function store(StoreTagRequest $request)
    {
        $tag = $this->tagRepository->store($request->only('name'));

        if ($tag)
            return $this->resultJson($tag, 200, 'OK');
    }

    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $tag = $this->tagRepository->update($request->only('name'), $tag);

        if ($tag)
            return $this->resultJson($tag, 200, 'OK');
    }

    public function delete(ManageTagRequest $request, Tag $tag)
    {
        $this->tagRepository->deleteById($tag->id);

        return $this->resultJson(null, 200, 'OK');
    }
}
