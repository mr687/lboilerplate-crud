<?php


namespace App\Models\Master\Traits\Relationship;


use App\Models\Master\Product;

trait TagRelationship
{
    public function products()
    {
        return $this->morphedByMany(Product::class, 'taggable');
    }
}
