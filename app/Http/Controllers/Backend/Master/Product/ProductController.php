<?php

namespace App\Http\Controllers\Backend\Master\Product;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Master\VendorRepository;
use Illuminate\Http\Request;
use App\Repositories\Backend\Master\ProductRepository;
use App\Http\Requests\Backend\Master\Product\ManageProductRequest;
use App\Http\Requests\Backend\Master\Product\StoreProductRequest;
use App\Http\Requests\Backend\Master\Product\UpdateProductRequest;
use App\Models\Master\Product;
use App\Events\Backend\Master\Product\ProductDeleted;

class ProductController extends Controller
{
    protected $productRepository;
    protected $vendorRepository;

    public function __construct(ProductRepository $productRepository,
                                VendorRepository $vendorRepository)
    {
        $this->productRepository = $productRepository;
        $this->vendorRepository = $vendorRepository;
    }

    public function index(ManageProductRequest $request)
    {
        return view('backend.master.product.index')
            ->withProducts($this->productRepository
            ->with('vendor', 'tags')
            ->orderBy('id')
            ->paginate(25));
    }

    public function create(ManageProductRequest $request)
    {
        return view('backend.master.product.create')
            ->withVendors($this->vendorRepository
            ->get());
    }

    public function store(StoreProductRequest $request)
    {
        $vendor = $this->vendorRepository->getById($request->vendor_id);
        $this->productRepository->store(
            $vendor,
            $request->only('name', 'price', 'tags')
        );

        return redirect()->route('admin.master.product.index')->withFlashSuccess('Product Created');
    }

    public function show(ManageProductRequest $request, Product $product)
    {

    }

    public function edit(ManageProductRequest $request, Product $product)
    {
        return view('backend.master.product.edit')
            ->withProduct($product)
            ->withVendors($this->vendorRepository->get());
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $this->productRepository->update($product, $request->only('name', 'price', 'vendor_id', 'tags'));

        return redirect()->route('admin.master.product.index')->withFlashSuccess('Product Updated');
    }

    public function destroy(ManageProductRequest $request, Product $product)
    {
        $this->productRepository->deleteById($product->id);

        event(new ProductDeleted($product));

        return redirect()->route('admin.master.product.index')->withFlashSuccess('Product Deleted');
    }
}
