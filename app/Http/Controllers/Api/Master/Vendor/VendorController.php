<?php

namespace App\Http\Controllers\Api\Master\Vendor;

use App\Http\Controllers\Api\Master\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Master\Vendor\ManageVendorRequest;
use App\Http\Requests\Api\Master\Vendor\StoreVendorRequest;
use App\Http\Requests\Api\Master\Vendor\UpdateVendorRequest;
use App\Models\Master\Vendor;
use App\Repositories\Backend\Master\VendorRepository;

class VendorController extends BaseController
{
    protected $vendorRepository;

    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    public function index(ManageVendorRequest $request)
    {
        return $this->resultJson(
            $this->vendorRepository
            ->with('products')
            ->orderBy('id')->paginate(25),
            200,
            'OK'
        );
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor)
    {
        $vendor = $this->vendorRepository->update(
            $vendor,
            $request->only('name', 'address')
        );
        if ($vendor)
            return $this->resultJson(
                $vendor, 200, 'OK'
            );
    }

    public function store(StoreVendorRequest $request)
    {
        $vendor = $this->vendorRepository->store($request->only('name', 'address'));
        if ($vendor)
            return $this->resultJson($vendor, 200, 'OK');
    }

    public function delete(ManageVendorRequest $request, Vendor $vendor)
    {
        $this->vendorRepository->deleteById($vendor->id);

        return $this->resultJson(null, 200, 'OK');
    }

}
