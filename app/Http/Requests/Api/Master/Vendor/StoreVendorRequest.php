<?php


namespace App\Http\Requests\Api\Master\Vendor;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreVendorRequest extends FormRequest
{

    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('vendors')],
            'address' => ['required']
        ];
    }
}
