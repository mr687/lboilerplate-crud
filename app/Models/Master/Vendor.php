<?php

namespace App\Models\Master;

use App\Models\Master\Traits\Relationship\VendorRelationship;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use VendorRelationship;

    protected $fillable = ['name', 'address', 'vendor_id'];
}
