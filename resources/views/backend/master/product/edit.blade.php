@extends('backend.layouts.app')

@section('title', 'Product Management | Edit Product')

@section('content')
    {{ html()->modelForm($product, 'PATCH', route('admin.master.product.update', $product))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Product Management
                        <small class="text-muted">Edit Product</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <!--row-->

            <hr />

{{--            @php dd() @endphp--}}

            <div class="row mt-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.roles.name'))
                            ->class('col-md-2 form-control-label')
                            ->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.roles.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Price')
                            ->class('col-md-2 form-control-label')
                            ->for('price') }}

                        <div class="col-md-10">
                            {{ html()->number('price')
                                ->class('form-control')
                                ->placeholder('Price')
                                ->attribute('maxlength', 191)
                                ->attribute('min', 0)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Vendor Name')
                            ->class('col-md-2 form-control-label')
                            ->for('vendor_id') }}

                        <div class="col-md-10">
                            <select name="vendor_id"
                                    id="vendor_id"
                                    required
                                    class="form-control">
                                <option value="">--Select Vendor Name--</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->id}}" @if($vendor->id == $product->vendor_id) selected @endif>{{$vendor->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label('Tags')
                            ->class('col-md-2 form-control-label')
                            ->for('tags') }}

                        <div class="col-md-10">
                            <input type="text" name="tags" class="form-control" placeholder="tag1,tag2,..." value="{{$product->tags->implode('name', ',')}}">
                            <small class="text-muted">Separate with comma (,)</small>
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.master.product.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
