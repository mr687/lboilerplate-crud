<?php

namespace App\Http\Controllers\Backend\Master\Tag;

use App\Events\Backend\Master\Tag\TagDeleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Master\Tag\ManageTagRequest;
use App\Http\Requests\Backend\Master\Tag\StoreTagRequest;
use App\Http\Requests\Backend\Master\Tag\UpdateTagRequest;
use App\Models\Master\Tag;
use App\Repositories\Backend\Master\TagRepository;

class TagController extends Controller
{
    protected $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function index(ManageTagRequest $request)
    {
        return view('backend.master.tag.index')
            ->withTags(
                $this->tagRepository
                ->orderBy('id')
                ->paginate(25)
            );
    }

    public function create(ManageTagRequest $request)
    {
        return view('backend.master.tag.create');
    }

    public function store(StoreTagRequest $request)
    {
        $this->tagRepository->store(
            $request->only('name')
        );

        return redirect()->route('admin.master.tag.index')->withFlashSuccess('Tag Created');
    }

    public function edit(ManageTagRequest $request, Tag $tag)
    {
        return view('backend.master.tag.edit')
            ->withTag($tag);
    }

    public function update(UpdateTagRequest $request, Tag $tag)
    {
        $this->tagRepository->update(
            $request->only('name'),
            $tag
        );

        return redirect()->route('admin.master.tag.index')->withFlashSuccess('Tag Updated');
    }

    public function destroy(ManageTagRequest $request, Tag $tag)
    {
        $this->tagRepository->deleteById($tag->id);

        event(new TagDeleted($tag));

        return redirect()->route('admin.master.tag.index')->withFlashSuccess('Tag Created');
    }
}
