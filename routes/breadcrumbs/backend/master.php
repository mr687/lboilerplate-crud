<?php

//Product Breadcrumbs
Breadcrumbs::for('admin.master.product.index', function ($trail) {
    $trail->push('Product Management', route('admin.master.product.index'));
});

Breadcrumbs::for('admin.master.product.create', function ($trail) {
    $trail->parent('admin.master.product.index');
    $trail->push('Create Product', route('admin.master.product.create'));
});

Breadcrumbs::for('admin.master.product.edit', function ($trail, $id) {
    $trail->parent('admin.master.product.index');
    $trail->push('Edit Product', route('admin.master.product.edit', $id));
});


//Vendor Breadcrumbs
Breadcrumbs::for('admin.master.vendor.index', function ($trail) {
    $trail->push('Vendor Management', route('admin.master.vendor.index'));
});

Breadcrumbs::for('admin.master.vendor.create', function ($trail) {
    $trail->parent('admin.master.vendor.index');
    $trail->push('Create Vendor', route('admin.master.vendor.create'));
});

Breadcrumbs::for('admin.master.vendor.edit', function ($trail, $id) {
    $trail->parent('admin.master.vendor.index');
    $trail->push('Edit', route('admin.master.vendor.edit', $id));
});

//Tag Breadcrumbs
Breadcrumbs::for('admin.master.tag.index', function ($trail) {
    $trail->push('Tag Management', route('admin.master.tag.index'));
});

Breadcrumbs::for('admin.master.tag.create', function ($trail) {
    $trail->parent('admin.master.tag.index');
    $trail->push('Create Tag', route('admin.master.tag.create'));
});

Breadcrumbs::for('admin.master.tag.edit', function ($trail, $id) {
    $trail->parent('admin.master.tag.index');
    $trail->push('Edit Tag', route('admin.master.tag.edit', $id));
});
