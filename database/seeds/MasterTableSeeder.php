<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\PermissionRegistrar;

class MasterTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        resolve(PermissionRegistrar::class)->forgetCachedPermissions();

        $this->truncateMultiple([
            'products',
            'tags',
            'vendors',
            'taggables',
        ]);

        $this->call(VendorTableSeeder::class);
        $this->call(TagTableSeeder::class);

        $this->enableForeignKeys();
    }
}
