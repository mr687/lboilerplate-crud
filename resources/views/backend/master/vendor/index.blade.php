@extends('backend.layouts.app')

@section('title', app_name() . ' | Vendor Management')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Vendor Management
                    </h4>
                </div><!--col-->

                <div class="col-sm-7 pull-right">
                    @include('backend.master.vendor.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Vendor Name</th>
                                <th>Address</th>
                                <th>Number of Products</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($vendors as $vendor)
                                <tr>
                                    <td>{{ ucwords($vendor->name) }}</td>
                                    <td>{{ $vendor->address }}</td>
                                    <td>{{ $vendor->products->count() }}</td>
                                    <td>@include('backend.master.vendor.includes.actions', ['vendor' => $vendor])</td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {!! $vendors->total() !!} products total
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $vendors->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->
    </div><!--card-->
@endsection
