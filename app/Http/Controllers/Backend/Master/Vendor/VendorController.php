<?php

namespace App\Http\Controllers\Backend\Master\Vendor;

use App\Events\Backend\Master\Vendor\VendorDeleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Master\Vendor\ManageVendorRequest;
use App\Http\Requests\Backend\Master\Vendor\StoreVendorRequest;
use App\Http\Requests\Backend\Master\Vendor\UpdateVendorRequest;
use App\Models\Master\Vendor;
use App\Repositories\Backend\Master\ProductRepository;
use App\Repositories\Backend\Master\VendorRepository;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    protected $vendorRepository;
    protected $productRepository;

    public function __construct(VendorRepository $vendorRepository,
                                ProductRepository $productRepository)
    {
        $this->vendorRepository = $vendorRepository;
        $this->productRepository = $productRepository;
    }

    public function index(ManageVendorRequest $request)
    {
        return view('backend.master.vendor.index')
            ->withVendors($this->vendorRepository
            ->orderBy('id')
            ->paginate(25));
    }

    public function create(ManageVendorRequest $request)
    {
        return view('backend.master.vendor.create');
    }

    public function store(StoreVendorRequest $request)
    {
        $this->vendorRepository->store(
          $request->only('name', 'address')
        );

        return redirect()->route('admin.master.vendor.index')->withFlashSuccess('Vendor Created');
    }

    public function show(ManageVendorRequest $request, Vendor $vendor)
    {

    }

    public function edit(ManageVendorRequest $request, Vendor $vendor)
    {
        return view('backend.master.vendor.edit')
            ->withVendor($vendor);
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor)
    {
        $this->vendorRepository->update($vendor, $request->only('name', 'address'));

        return redirect()->route('admin.master.vendor.index')->withFlashSuccess('Vendor Updated');
    }

    public function destroy(ManageVendorRequest $request, Vendor $vendor)
    {
        foreach ($vendor->products as $product)
        {
            $this->productRepository->deleteById($product->id);
        }

        $this->vendorRepository->deleteById($vendor->id);

        event(new VendorDeleted($vendor));

        return redirect()->route('admin.master.vendor.index')->withFlashSuccess('Vendor Deleted');
    }
}
