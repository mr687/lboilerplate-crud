<?php

namespace App\Listeners\Backend\Master\Tag;

use App\Events\Backend\Master\Tag\TagCreated;
use App\Events\Backend\Master\Tag\TagDeleted;
use App\Events\Backend\Master\Tag\TagUpdated;

class TagEventListener
{
    public function onCreated($event)
    {
        logger('Tag Created');
    }

    public function onUpdated($event)
    {
        logger('Tag Updated');
    }

    public function onDeleted($event)
    {
        logger('Tag Deleted');
    }

    public function subcribe($events)
    {
        $events->listen(
            TagCreated::class,
            'App\Listeners\Backend\Master\Tag\TagEventListener@onCreated'
        );

        $events->listen(
            TagUpdated::class,
            'App\Listeners\Backend\Master\Tag\TagEventListener@onUpdated'
        );

        $events->listen(
            TagDeleted::class,
            'App\Listeners\Backend\Master\Tag\TagEventListener@onDeleted'
        );
    }
}
