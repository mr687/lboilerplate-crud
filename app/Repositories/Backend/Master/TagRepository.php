<?php


namespace App\Repositories\Backend\Master;


use App\Events\Backend\Master\Tag\TagCreated;
use App\Events\Backend\Master\Tag\TagDeleted;
use App\Events\Backend\Master\Tag\TagUpdated;
use App\Exceptions\GeneralException;
use App\Models\Master\Tag;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class TagRepository extends BaseRepository
{
    public function __construct(Tag $model)
    {
        $this->model  = $model;
    }

    public function store(array $data): Tag
    {
        if ($this->tagExists($data['name']))
        {
            throw new GeneralException('A tag already exists with the name '.e($data['name']));
        }

        return DB::transaction(function() use($data) {
            $tag = $this->model->create($data);
            if ($tag)
            {
                event(new TagCreated($tag));
                return $tag;
            }

           throw new GeneralException('Something wrong!');
        });
    }

    public function update(array $data, Tag $tag): Tag
    {
        if ($tag->name != $data['name'])
        {
            if ($this->tagExists($data['name']))
            {
                throw new GeneralException('A tag already exists with the name '.e($data['name']));
            }
        }

        return DB::transaction(function () use($data, $tag) {
            $tag->update($data);
            if ($tag)
            {
                event(new TagUpdated($tag));

                return $tag;
            }

            throw new GeneralException('Something wrong!');
        });
    }

    public function deleteById($id)
    {
        $tag = $this->getById($id);

        foreach ($tag->products as $product)
        {
            $product->tags()->detach($id);
        }

        return parent::deleteById($id);
    }

    protected function tagExists($name): bool
    {
        return $this->model
            ->where('name', strtolower($name))
            ->count() > 0;
    }

}
